import { initReactI18next } from 'react-i18next';
import * as RNLocalize from 'react-native-localize';
import i18n, { LanguageDetectorModule } from 'i18next';

import { resources } from '../config';

const getLocale = (): string => {
  const bestAvailable = RNLocalize.findBestAvailableLanguage(Object.keys(resources));

  return bestAvailable?.languageTag || 'en';
};

const languageDetector: LanguageDetectorModule = {
  type: 'languageDetector',
  detect: getLocale,
  init: () => {},
  cacheUserLanguage: () => {},
};

export const initI18N = () =>
  i18n
    .use(languageDetector)
    .use(initReactI18next)
    .init(
      {
        compatibilityJSON: 'v3',
        fallbackLng: 'en',
        resources,
        interpolation: {
          escapeValue: false,
        },
      },
      err => {
        if (err) return console.warn('something went wrong loading', err);
      }
    );

export const changeLanguage = (language: string | undefined) => {
  return i18n.changeLanguage(language || getLocale());
};

export const getCurrentLanguage = (): string => {
  return i18n.language;
};

export { i18n };
