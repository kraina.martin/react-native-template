import React, { useEffect, useState } from 'react';
import * as RNLocalize from 'react-native-localize';

import { changeLanguage, initI18N } from '../services/i18n';

const LocalizationProvider = React.memo<{ children: React.ReactElement }>(props => {
  const [isInitiated, setIsInitiated] = useState(false);
  useEffect(() => {
    const init = async () => {
      try {
        await initI18N();
        const [currentLanguage] = RNLocalize.getLocales();
        await changeLanguage(currentLanguage?.languageCode);
      } finally {
        setIsInitiated(true);
      }
    };
    void init();
  }, []);
  if (!isInitiated) return null;
  return props.children;
});

export default LocalizationProvider;
