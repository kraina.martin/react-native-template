import React from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, Text, View } from 'react-native';

import { LocalizationProvider } from '~src/i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const App = React.memo(() => {
  const { t } = useTranslation();
  return (
    <View style={styles.container}>
      <Text>{t('welcomeMessage')}</Text>
    </View>
  );
});

export default React.memo(() => (
  <LocalizationProvider>
    <App />
  </LocalizationProvider>
));
