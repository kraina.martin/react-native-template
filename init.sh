#!/bin/bash
APP_NAME=$1
if [ -z "$APP_NAME" ]; then 
    echo "Error: app name not specified"
    exit 1
fi
if [[ ! "$APP_NAME" != *[^[:alnum:]/]* ]]; then
   echo "Error: \"$APP_NAME\" is not a valid name for a project. Please use a valid identifier name (alphanumeric)."
   exit 1
fi
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd ..
echo "react-native init $APP_NAME"
yes | npx react-native init $APP_NAME --template react-native-template-typescript
cd $APP_NAME
echo "install eslint plugins"
yarn add -D prettier @react-native-community/eslint-config @shopify/eslint-plugin @typescript-eslint/eslint-plugin @typescript-eslint/parser eslint eslint-config-prettier eslint-import-resolver-typescript eslint-plugin-import eslint-plugin-jest eslint-plugin-prefer-arrow eslint-plugin-prettier eslint-plugin-react eslint-plugin-react-memo eslint-plugin-react-perf eslint-plugin-simple-import-sort eslint-plugin-typescript-enum eslint-plugin-typescript-sort-keys
echo "prepare project structure"
rsync -r -av $SCRIPT_DIR/.??* ./ --exclude=init.sh --exclude=.DS_Store --exclude=.git
rsync -r -av $SCRIPT_DIR/* ./ --exclude=init.sh --exclude=.DS_Store
rm App.tsx
rm -rf __tests__
yarn add -D babel-plugin-module-resolver
echo "add scripts"
 npm set-script verify "yarn run tsc && yarn run lint --quiet && yarn run test" -f
 npm set-script lint "eslint . --ext .ts,.tsx --cache" -f
 npm set-script clean "watchman watch-del-all && rm -rf \$TMPDIR/metro-cache/  && rm -rf \$TMPDIR/haste-map-metro*  && rm -rf node_modules/ && yarn cache clean && yarn" -f
 npm set-script pods "cd ios && pod install && cd .."
 npm set-script test "jest --passWithNoTests" -f
echo "prepare git"
git init .
yarn add -D lint-staged
yarn add -D husky
npm set-script prepare "husky install; yes | npx pod-install"
yarn prepare
npx husky add .husky/pre-commit "yarn lint-staged"
npx husky add .husky/pre-push "yarn verify"
echo  >> .gitignore 
echo '#ESlint' >> .gitignore 
echo '.eslintcache' >> .gitignore 
echo '#Jest' >> .gitignore
echo '.jest/cache' >> .gitignore
echo "add i18n"
yarn add -D i18next react-i18next react-native-localize